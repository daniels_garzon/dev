<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>United States</label>
    <protected>false</protected>
    <values>
        <field>FieldCode__c</field>
        <value xsi:type="xsd:string">US</value>
    </values>
    <values>
        <field>FieldNumber__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>FieldValue__c</field>
        <value xsi:type="xsd:string">United States</value>
    </values>
    <values>
        <field>ObjectType__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
